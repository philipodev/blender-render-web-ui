import numpy as np
import gradio as gr
import os

with gr.Blocks() as demo:
	gr.Markdown("## Blender Render")

	with gr.Row():
		with gr.Tab("General"):
			input_file = gr.File(label="Input File", file_types=[".blend"])
			render_type = gr.Radio(choices=[("Single Image", "single"), ("Animation", "animation")], label="Render Type", value="single")

			with gr.Row():
				single_frame = gr.Number(label="Frame", value=0)
				animation_start = gr.Number(label="Start", value=0)
				animation_end = gr.Number(label="End", value=0)

			render_button = gr.Button(value="Render", variant="primary")
		with gr.Tab("Script"):
			gr.Markdown("### Python Script\nRun python code inside the blender project")
			python_expr = gr.Code(language="python", label="Python Code", lines=25)
		
		with gr.Column() as output_image_column:
			output_file = gr.Image(label="Result", interactive=False)

		with gr.Column(visible=False) as output_video_column:
			output_video = gr.Video(label="Result", interactive=False)

	def submit(
			input_file,
			render_type,
			single_frame,
			animation_start,
			animation_end,
			python_expr
		):
		print(input_file.name)

		blender_path = os.environ.get("BLENDER_PATH", "\"C:\\Program Files\\Blender Foundation\\Blender 4.0\\blender.exe\"")

		options = {
			"b": input_file.name,
			"o": "/tmp/output",
			"F": "PNG",
			"f": "0",
			"x": "1",
		}

		output_file_name = "/tmp/output0000.png"

		if render_type == "single":
			options["f"] = str(int(single_frame))
		else:
			options["a"] = ""
			options["s"] = str(int(animation_start))
			options["e"] = str(int(animation_end))
			options["F"] = "MPEG"

		command = f"{blender_path}"
		for key, value in options.items():
			command += f" -{key} {value}"


		os.system(command)

		if render_type == "animation":
			return {
				output_image_column: gr.Column(visible=False),
				output_video_column: gr.Column(visible=True),
				output_video: "/tmp/output0001-0024.mp4"
			}

		return {
			output_file: output_file_name,
		}


	render_button.click(submit, [
		input_file, 
		render_type, 
		single_frame,
		animation_start,
		animation_end,
		python_expr
	], [output_file, output_video, output_image_column, output_video_column])

demo.launch(share=True)
