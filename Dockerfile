FROM thebloke/ubuntu2204-cuda120-venv

RUN apt-get update && apt-get install -y \
    blender

WORKDIR /app

RUN pip3 install gradio

COPY app.py .

EXPOSE 7860

CMD ["python3", "app.py"]
